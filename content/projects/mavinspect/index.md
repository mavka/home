+++
menus = 'main'
title = 'MAVinspect'
description = 'A Rust library for parsing MAVLink XML definitions written in Rust.'
summary = 'A Rust library for parsing MAVLink XML definitions written in Rust.'
images = ['/home/projects/mavinspect/mavinspect-social-large.png']
+++
{{< columns >}}

MAVInspect
==========

A Rust library for parsing [MAVLink](https://mavlink.io/en/) XML definitions.
<--->
<div style="text-align: center">
<img src="mavinspect-196.png" alt="MAVInspect" title="MAVInspect">
</div>
{{< /columns >}}

[`repository`](https://gitlab.com/mavka/libs/mavinspect)
[`crates.io`](https://crates.io/crates/mavinspect)
[`API docs`](https://docs.rs/mavinspect/latest/mavinspect/)
[`issues`](https://gitlab.com/mavka/libs/mavinspect/-/issues)

This library is a building block for other MAVLink-related tools (code generators, telemetry collectors, IO, etc.). We
intentionally moved everything which is not related to protocol inspection to other [Mavka](https://mavka.gitlab.io/home/)
project.

Install
-------

Install MAVSpec with cargo:

```shell
cargo add mavspec
```

Usage
-----

Parse standard and custom XML definitions from `./message_definitions`:

```rust
use std::env;

use mavinspect::parser::Inspector;

fn main() {
    // Instantiate inspector and load list of XML definitions
    let inspector = Inspector::builder()
        .set_sources(vec![
            "./message_definitions/standard".to_string(),
            "./message_definitions/extra".to_string(),
        ])
        .build()
        .unwrap();
    
    // Parse all XML definitions
    let protocol = inspector.parse().unwrap();
    
    // Get `crazyflight` custom dialect
    let crazyflight = protocol.dialects().get("crazy_flight").unwrap();
    
    // Get `CRAZYFLIGHT_OUTCRY` message
    let outcry_message = crazyflight.messages().get(&54000u32).unwrap();
    assert_eq!(outcry_message.name(), "CRAZYFLIGHT_OUTCRY");
    println!("\n`CRAZYFLIGHT_OUTCRY` message: {:#?}", outcry_message);
}
```

See [examples](https://gitlab.com/mavka/libs/mavinspect/-/tree/main/examples?ref_type=heads) and
[`API documentation`](https://docs.rs/mavinspect/latest/mavinspect/) for advanced usage.
