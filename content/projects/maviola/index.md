+++
menus = 'main'
title = 'Maviola'
description = 'Maviola is a high-level MAVLink communication library written in Rust.'
summary = 'Maviola is a high-level MAVLink communication library written in Rust.'
images = ['/home/projects/maviola/maviola-social-large.png']
+++
{{< columns >}}
Maviola
=======
A high-level [MAVLink](https://mavlink.io/en/) communication library based on [`Mavio`](../../projects/mavio).
<--->
<div style="text-align: center">
<img src="maviola-196.png" alt="Maviola" title="Maviola">
</div>
{{< /columns >}}

[`repository`](https://gitlab.com/mavka/libs/maviola)
[`crates.io`](https://crates.io/crates/maviola)
[`API docs`](https://docs.rs/maviola/latest/maviola/)
[`issues`](https://gitlab.com/mavka/libs/maviola/-/issues)

Features
--------

Maviola is a high-level library that contains abstractions such as communication nodes, networks, and MAVLink devices.
It extends [`Mavio`](../../projects/mavio) by hiding all the complexity related to maintaining MAVLink network.

* Supports both `MAVLink 1` and `MAVLink 2` protocol versions.
* Provides intermediate MAVLink packets decoding as "frames" that contain only header, checksum and signature being
  deserialized. Which means that client don't have to decode the entire message for routing and verification.
* Provides both synchronous and asynchronous API.
* Automatically sends heartbeats and monitors devices within MAVLink networks.
* Includes predefined transports for TCP, UDP, Unix-sockets, and files.
* Includes standard MAVLink dialects enabled by Cargo features.
* Implements message verification via checksum.
* Includes tools for [message signing](https://mavlink.io/en/guide/message_signing.html).
* Supports additional MAVLink dialect generated by [MAVSpec](https://gitlab.com/mavka/libs/mavspec) from MAVLink
  message definition XML format and even allows to create ad-hoc MAVLink dialects using pure Rust.

By being interoperable with [`Mavio`](../../projects/mavio) that was designed primarily for `no_std` targets and
embedded devices, Maviola server as a natural extension that can be used on companion computers or ground station
infrastructure.

Install
-------

Install Maviola by `cargo` with synchronous API support:

```shell
cargo add maviola --features sync
```

Usage
-----

For details, please check [API documentation](https://docs.rs/maviola/latest/maviola/) or jump direct to the
[Quickstart](https://docs.rs/maviola/latest/maviola/docs/a1__quickstart/index.html).

### Sending and Receiving MAVLink Messages

Create a TCP server, respond to incoming events, broadcast incoming frames:

```rust
use maviola::prelude::*;
use maviola::sync::prelude::*;

fn main() -> Result<()> {
  // Create a synchronous MAVLink node 
  // with `MAVLink 2` protocol version
  let server = Node::sync::<V2>()
          // Set device system and component IDs
          .id(MavLinkId::new(17, 42))
          // Define connection settings
          .connection(TcpServer::new("127.0.0.1:5600")?) 
          .build()?;

  // Handle node events
  for event in server.events() {
    match event {
      // Handle a new peer
      Event::NewPeer(peer) => println!("new peer: {peer:?}"),
      // Handle a peer that becomes inactive
      Event::PeerLost(peer) => {
        println!("peer offline: {peer:?}");
        // Exit when all peers are disconnected
        if !server.has_peers() {
          break;
        }
      }
      // Handle incoming MAVLink frame
      Event::Frame(frame, callback) => if server.validate_frame(&frame).is_ok() {
        // Handle heartbeat message
        if let Ok(Minimal::Heartbeat(msg)) = frame.decode::<Minimal>() {
          // Respond with the same heartbeat message to all clients,
          // except the one that sent this message
          callback.respond_others(&server.next_frame(&msg)?)?;
        }
      }
      Event::Invalid(frame, err, callback) => {
        /* Handle invalid frame */
      }
    }
  }
}
```

A detailed breakdown of this example can be found in the [Quickstart](https://docs.rs/maviola/latest/maviola/docs/a1__quickstart/index.html)
chapter of the [Maviola Playbook](https://docs.rs/maviola/latest/maviola/docs/index.html).

### Handling Multiple Connections

Create a network that consists TCP server and UDP client. Take unsigned messages from one network, sign them and pass
them to another:

```rust
use maviola::prelude::*;
use maviola::sync::prelude::*;

fn main() -> Result<()> {
    // Create a synchronous MAVLink node 
    // with `MAVLink 2` as protocol version
    // and two connections
    let node = Node::sync::<V2>()
        .connection(Network::sync()
            .add_node(
                // This node communicates with trusted network,
                // where there is no reason to sign messages
                Node::sync()
                    .connection(TcpServer::new("127.0.0.1:5600").unwrap())
                    .signer(FrameSigner::builder()
                        .link_id(11)
                        .key("secure key")
                        // Removes signature from outgoing frames
                        .outgoing(SignStrategy::Strip)
                        // Signs incoming frames    
                        .incoming(SignStrategy::Sign)
                    )
            )
            .add_node(
                // And this one connects to unsecure network,
                // where messages should be signed
                Node::sync()
                    .connection(UdpClient::new("10.98.0.1:14550").unwrap())
                    .signer(FrameSigner::builder()
                        .link_id(11)
                        .key("secure key")
                        // Ensures that outgoing messages are signed
                        .outgoing(SignStrategy::Sign)
                        // Accepts only signed messages    
                        .incoming(SignStrategy::Strict)
                    )
            )
        )
        .build().unwrap();
}
```

Check the [Networks & Routing](https://docs.rs/maviola/latest/maviola/docs/a2__overview/index.html#networks--routing)
section in the [Maviola Playbook](https://docs.rs/maviola/latest/maviola/docs/index.html) for details.

API Notes
---------

Maviola provides both [synchronous](https://docs.rs/maviola/latest/maviola/docs/a3__sync_api/index.html) and
[asynchronous](https://docs.rs/maviola/latest/maviola/docs/a4__async_api/index.html) APIs. Check
[Choosing Your API](https://docs.rs/maviola/latest/maviola/docs/a2__overview/index.html#choosing-your-api) section to
learn more about the difference, advantages, and downsides of each type of API.

### I/O

Maviola support TCP, UDP, Unix-sockets, and files as built-in transports. Custom transports can be constructed from
anything that read and write to byte buffers.

### Custom Dialects

Concrete implementations of dialects are generated by [MAVSpec](https://gitlab.com/mavka/libs/mavspec) check its
[documentation](https://docs.rs/mavspec/latest/mavspec/) for details. You may also found useful to review
[`build.rs`](https://gitlab.com/mavka/libs/mavio/-/blob/main/mavio/build.rs?ref_type=heads) from Mavio, if you want to
generate your custom dialects. Maviola inherits canonical dialects from this library.

### Ad-hoc Dialects

It is possible to define MAVLink dialects using pure Rust. The
[Ad-hoc Dialects](https://docs.rs/maviola/latest/maviola/docs/c4__ad_hoc_dialects/index.html) chapter of
[Maviola Playbook](https://docs.rs/maviola/latest/maviola/docs/index.html) provides a basic example. You can learn
more about ad-hoc dialects from [MAVSpec](https://docs.rs/mavspec/latest/mavspec/) documentation.
