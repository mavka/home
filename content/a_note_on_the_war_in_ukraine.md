+++
menus = 'main'
title = 'A Note on the War in Ukraine'
description = "Creating an open-source project is one of the many ways to affect the World around you. This is neither a small thing nor a big thing. It's a decent thing. What is the order of thing which enables us to engage into public action? How does this war threaten such order?"
summary = "Creating an open-source project is one of the many ways to affect the World around you. This is neither a small thing nor a big thing. It's a decent thing. What is the order of thing which enables us to engage into public action? How does this war threaten such order?"
+++
A Note on the War in Ukraine
============================

Creating an open-source project is one of the many ways to affect the World around you. This is neither a small thing
nor a big thing. It's a decent thing. Human decency can shape the World, but it draws power from the order of things,
such as law, minimal trust in strangers, and common sense. If such conditions are threatened, decency becomes
impossible. Decent acts turn into small deeds, dramatic gestures, or heroic actions.

The war Russia started and the way it conducts it is incompatible with the international order. When a member of the UN
Security Council invades another country and proclaims a program of cultural and physical genocide, it's not just a
crime; it's an act that threatens the possibility of the law itself.

This project was initiated and mainly written in Kyiv under the constant threat of missile strikes. As a fair witness,
I must admit that the intensity of such strikes and the danger they pose to my life is incomparable to what my fellow
citizens are facing in the southern and eastern parts of Ukraine. I don't want to focus on my experience of such events.
After all, experience is overrated. I initially thought to put a link to a charity foundation I consider efficient and
trustworthy, but something stopped me. Instead, I am offering to think about the order of things compatible with a
decent way of life and the threat this soon-to-be ten-year war poses to such order.

Ask yourself: What should a prudent political act look like under such circumstances? No matter how sincere they are,
charity and good intentions can't win this war, while a consistent political strategy can (I do a lot of charity and
know what I am saying). If you find yourself equally incompatible with this war as I do, consider acting politically,
taking a side, and articulating your position. Make your government win the right war.
