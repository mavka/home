+++
menus = 'main'
title = 'Mavka'
description = 'Mavka is a collection of tools to parse, store, and communicate MAVLink data written in Rust.'
summary = 'Mavka is a collection of tools to parse, store, and communicate MAVLink data written in Rust.'
images = ['images/mavka-social-large.png']
+++
{{< columns >}}
![Mavka](images/mavka-256.png "Mavka")
<--->
Mavka
=====
is a collection of tools to parse, store, and communicate [MAVLink](https://mavlink.io/en/) data written in [Rust](https://www.rust-lang.org/).
{{< /columns >}}

{{< columns >}}

## [`Maviola`](projects/maviola)
A high-level [MAVLink](https://mavlink.io/en/) communication
library based on [`Mavio`](projects/mavio).

<--->

## [`Mavio`](projects/mavio)
A minimalistic library for transport-agnostic
[MAVLink](https://mavlink.io/en/) communication.

<--->

## [`MAVSpec`](projects/mavspec)
A code-generator for [MAVLink](https://mavlink.io/en/).

{{< /columns >}}

Why Mavka?
----------

We want to create a robust modular MAVLink toolchain allowing to use Rust both on embedded device, companion computers,
and over the communication infrastructure. The key pieces are:

- [MAVSpec](projects/mavspec) for code generation and ad-hoc dialect construction.
- [Mavio](projects/mavio) for transport-agnostic MAVLink I/O with emphasis on embedded devices.
- [Maviola](projects/maviola) for high-level MAVLink communication with abstractions like communication nodes, networks,
  routing, and MAVLink devices.

Our goal is to make Rust ecosystem a compelling choice for UAV-related projects both on the edge devices and at the
ground station level. We hope that our work will be helpful for those who are already using Rust in their MAVLink
infrastructure and those who are considering to adopt Rust for their next endeavour.

MAVLink
-------

MAVLink is a lightweight open protocol for communicating between drones, onboard components and ground control stations.
It is used by such autopilots like [PX4](https://px4.io) or [ArduPilot](https://ardupilot.org/#). MAVLink has simple and
compact serialization model. The basic abstraction is `message` which can be sent through a link (UDP, TCP, UNIX
socket, UART, whatever) and deserialized into a struct with fields of primitive types or arrays of primitive types.
Such fields can be additionally restricted by `enum` variants, annotated with metadata like units of measurements,
default or invalid values.

There are several MAVLink dialects. Official dialect definitions are
[XML files](https://mavlink.io/en/guide/xml_schema.html) that can be found in the MAVlink
[repository](https://github.com/mavlink/mavlink/tree/master/message_definitions/v1.0). Based on `message` abstractions,
MAVLink defines so-called [`microservices`](https://mavlink.io/en/services/) that specify how clients should respond on
a particular message under certain conditions or how they should initiate a particular action.

### Other MAVLink Tools

First of all, there is an official MAVLink client for Rust worth mentioning:
[`rust-mavlink`](https://github.com/mavlink/rust-mavlink). One of the reasons behind writing *this* library was the desire
to decouple parser and code generator into the separate projects.

We were personally inspired by [`gomavlib`](https://github.com/bluenviron/gomavlib) library for MAVLink (Go). We like the
way it is written, and its source code helped us in several cases when official MAVLink documentation wasn't clear enough.

If you want to autogenerate language bindings and prefer Python, you might be interested in the official
[`mavgen`](https://mavlink.io/en/getting_started/generate_libraries.html#mavgen) code-generation tool. If you are
looking for a router for MAVLink messages, then we suggest [`mavp2p`](https://github.com/bluenviron/mavp2p). If
you want a solution that supports MAVLink microservices, then it worth checking [`MAVSDK`](https://github.com/mavlink/MAVSDK)
that uses [gRPC](https://grpc.io) API.

MAVLink is almost 15 years old, but the ecosystem around this protocol is still dynamic and developing. Some projects are
stable and robust, while others are nice and feature-rich but incomplete.

License
-------

Following [Rust API Guidelines](https://rust-lang.github.io/api-guidelines/about.html), all project within Mavka family
are licensed under either of

* Apache License, Version 2.0
  ([LICENSE-APACHE](https://gitlab.com/mavka/gitlab-profile/-/blob/main/LICENSE-APACHE?ref_type=heads) or
  http://www.apache.org/licenses/LICENSE-2.0)
* MIT license
  ([LICENSE-MIT](https://gitlab.com/mavka/gitlab-profile/-/blob/main/LICENSE-MIT?ref_type=heads) or
  http://opensource.org/licenses/MIT)

You can choose the option that suits you best.
